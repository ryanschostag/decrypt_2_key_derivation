"""
Script that decrypts using 2 key derivation
"""
import base64
import argparse
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Hash import SHA512
from Crypto.Random import get_random_bytes
import settings


def options():
	arguments = argparse.ArgumentParser(prog='2-Key Derivation Decryption')
	arguments.add_argument('password', help='Provide a password to decrypt')
	arguments.add_argument('--hash', help='Provide the hash')
	return arguments


def main():
	args = options()
	help_func = args.print_help 
	args = args.parse_args()

	try:
		password = args.password.encode(settings.encoding)

		if not args.hash:
			salt = get_random_bytes(16)
		else:
			salt = args.hash.encode(settings.encoding)

		keys = PBKDF2(password, salt, 64, count=1000000, hmac_hash_module=SHA512)
		key1 = keys[:32].decode('unicode_escape')
		key2 = keys[32:].decode('unicode_escape')
		print('Key #1: {k1}'.format(k1=key1))
		print('Key #2: {k1}'.format(k1=key1))
	except Exception as error:
		print(error)
		help_func()


if __name__ == "__main__":
	main()

