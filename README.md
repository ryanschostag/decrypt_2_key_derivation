# DECRYPT 2-KEY DERIVATION #

This tool assists with 2-key derivation decryption

## Requirements ## 

* Python 3.8.x

### 3rd-party Python Packages ### 

* pycryptodome

### Setup ###

Commands to run:

* cd ./decrypt_2_key_derivation
* pipenv sync --sequential
* pipenv shell
* python decrypt.py "password" --hash "hash"

### Usage ###

```

usage: 2-Key Derivation Decryption [-h] [--hash HASH] password

positional arguments:
  password     Provide a password to decrypt

optional arguments:
  -h, --help   show this help message and exit
  --hash HASH  Provide the hash
```

### Support ###

* Ryan Schostag, ryan.schostag@gmail.com
